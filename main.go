package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
	"quizz/app"
	quizz "quizz/domain"
	"time"
)

var (
	maxTimeoutInSeconds = 30
	timeDuration        = time.Duration(maxTimeoutInSeconds) * time.Second
)

func main() {
	flag.Int("Timeout duration", maxTimeoutInSeconds, "The time in seconds before the game ends")
	csvFilename := flag.String("csv", "problems.csv", "A csv file in the format of 'question, answer'")
	shuffle := flag.Bool("shuffle", true, "Whether the questions should be shuffle or not")
	_ = shuffle
	flag.Parse()

	file, fileErr := os.Open(*csvFilename)
	if fileErr != nil {
		fmt.Printf("Failed to open the CSV file: %v\n", fileErr)
		panic("Couldn't import the problems")
	}

	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("Error when closing file %s", err)
		}
	}()

	reader := csv.NewReader(file)
	ps := app.NewProblemsStruct(reader)
	records, importErr := ps.Import()
	if importErr != nil {
		fmt.Printf("Failed to open the CSV file: %v\n", importErr)
		panic("Couldn't import the problems")
	}

	game, buildErr := quizz.BuildQuizz(records)
	if buildErr != nil {
		panic("Error while building quizz game")
	}

	finished := make(chan bool, 1)

	go playGame(game, finished)

	select {
	case <-finished:
		fmt.Printf("You scored: %d\n", game.Score)
		fmt.Printf("Number of incorrect answers: %d\n", len(game.QuestionsAndAnwers)-game.Score)
		fmt.Printf("And finished the quizz in %d seconds\n", int64(time.Now().Sub(game.StartedAt).Seconds()))
	case <-time.After(timeDuration):
		fmt.Printf("You scored: %d\n", game.Score)
		fmt.Printf("Number of incorrect answers: %d\n", len(game.QuestionsAndAnwers)-game.Score)
		fmt.Printf("End of the game, it took too much time (> %d seconds)\n", int64(timeDuration.Seconds()))
		os.Exit(1)
	}

}

func playGame(game *quizz.Quizz, finishedCn chan<- bool) {
	fmt.Println("Push enter to start the game")
	fmt.Scanf("\n")

	game.Shuffle()
	game.StartedAt = time.Now()

	var answer string
	for i, qa := range game.QuestionsAndAnwers {
		fmt.Printf("%d. Question: %s?\n", i+1, qa.Question)
		fmt.Scanf("%s\n", &answer)

		if answer == qa.Answer {
			fmt.Println("Correct answer")
			game.Increment()
		}
	}
	finishedCn <- true
	return
}
