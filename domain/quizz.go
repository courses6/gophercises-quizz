package quizz

import (
	"math/rand"
	"time"
)

// QuestionAndAnwer ...
type QuestionAndAnwer struct {
	Question string
	Answer   string
}

// Quizz ...
type Quizz struct {
	QuestionsAndAnwers []QuestionAndAnwer
	Score              int
	StartedAt          time.Time
}

// Increment increments the quizz score
func (q *Quizz) Increment() int {
	q.Score++
	return (q.Score)
}

// GetScore returns the current score of the quizz
func (q *Quizz) GetScore() int {
	return q.Score
}

// Shuffle returns a shuffled slice of question&answers
func (q *Quizz) Shuffle() {
	for i := 1; i < len(q.QuestionsAndAnwers); i++ {
		r := rand.Intn(i + 1)
		if i != r {
			q.QuestionsAndAnwers[r], q.QuestionsAndAnwers[i] = q.QuestionsAndAnwers[i], q.QuestionsAndAnwers[r]
		}
	}
}
