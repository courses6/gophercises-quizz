package quizz

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuildQuizzWithWhitespaces(t *testing.T) {
	qas := [][]string{
		{"question 1?     ", "answer 1    "},
		{"     question 2?    ", "  answer 2"},
	}
	quizz, err := BuildQuizz(qas)
	assert.Nil(t, err)
	assert.NotNil(t, quizz)
	assert.EqualValues(t, "question 1?", quizz.QuestionsAndAnwers[0].Question)
	assert.EqualValues(t, "answer 1", quizz.QuestionsAndAnwers[0].Answer)
	assert.EqualValues(t, "question 2?", quizz.QuestionsAndAnwers[1].Question)
	assert.EqualValues(t, "answer 2", quizz.QuestionsAndAnwers[1].Answer)
}

func TestBuildQuizzWithEmptyQuestionOrAnswer(t *testing.T) {
	qas := [][]string{
		{"", "answer 1    "},
	}
	quizz, err := BuildQuizz(qas)
	assert.Nil(t, quizz)
	assert.NotNil(t, err)
	assert.EqualValues(t, "Error while constructing the quizz struct", err.Error())
}

func TestBuildQuizzWithValidQuestionOrAnswer(t *testing.T) {
	qas := [][]string{
		{"question 1?", "answer 1"},
		{"question 2?", "answer 2"},
	}
	quizz, err := BuildQuizz(qas)
	assert.Nil(t, err)
	assert.NotNil(t, quizz)
	assert.EqualValues(t, "question 1?", quizz.QuestionsAndAnwers[0].Question)
	assert.EqualValues(t, "answer 1", quizz.QuestionsAndAnwers[0].Answer)
	assert.EqualValues(t, "question 2?", quizz.QuestionsAndAnwers[1].Question)
	assert.EqualValues(t, "answer 2", quizz.QuestionsAndAnwers[1].Answer)
}
