package quizz

import (
	"errors"
	"strings"
)

// BuildQuizz ...
func BuildQuizz(records [][]string) (*Quizz, error) {
	var qa QuestionAndAnwer
	qas := make([]QuestionAndAnwer, len(records))
	for i, record := range records {
		question := strings.TrimSpace(record[0])
		answer := strings.TrimSpace(record[1])
		if question == "" || answer == "" {
			return nil, errors.New("Error while constructing the quizz struct")
		}
		qa = QuestionAndAnwer{
			Question: question,
			Answer:   answer,
		}
		qas[i] = qa
	}
	quizz := &Quizz{
		QuestionsAndAnwers: qas,
	}
	return quizz, nil
}
