package app

import (
	"errors"
	"io"
)

// ProblemsStruct ...
type ProblemsStruct struct {
	reader ProblemsReader
}

// ProblemsReader ...
type ProblemsReader interface {
	Read() (record []string, err error)
	ReadAll() (records [][]string, err error)
}

// NewProblemsStruct ...
func NewProblemsStruct(reader ProblemsReader) ProblemsStruct {
	return ProblemsStruct{reader}
}

// Import ...
func (fact *ProblemsStruct) Import() ([][]string, error) {
	var records [][]string
	for {
		elements, err := fact.reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		if len(elements) < 2 {
			return nil, errors.New("A line doesn't contain enough elements")
		}
		for _, element := range elements {
			if element == "" {
				return nil, errors.New("A question or an answer is empty")
			}
		}

		records = append(records, []string{elements[0], elements[1]})
	}

	return records, nil
}
