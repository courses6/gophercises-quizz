package app

import (
	"errors"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

type MockProblemsReaderStruct struct {
	counter int
	lines   int
	Call    func() (record []string, err error)
	CallAll func() (record [][]string, err error)
}

func (f *MockProblemsReaderStruct) Read() (record []string, err error) {
	for f.counter < f.lines {
		f.counter++
		return f.Call()
	}
	return nil, io.EOF
}

func (f *MockProblemsReaderStruct) ReadAll() (record [][]string, err error) {
	return f.CallAll()
}

var (
	mock *MockProblemsReaderStruct
)

func TestImportEmptyFile(t *testing.T) {
	// initialize
	mock = &MockProblemsReaderStruct{lines: 0}
	mock.Call = func() (record []string, err error) {
		return nil, io.EOF
	}

	// execute
	importer := NewProblemsStruct(mock)
	records, err := importer.Import()

	// assert
	assert.Nil(t, records)
	assert.Nil(t, err)
}

func TestImportReadError(t *testing.T) {
	// initialize
	mock = &MockProblemsReaderStruct{lines: 1}
	mock.Call = func() (record []string, err error) {
		return nil, errors.New("this line has an error")
	}

	// execute
	importer := NewProblemsStruct(mock)
	records, err := importer.Import()

	// assert
	assert.Nil(t, records)
	assert.NotNil(t, err)
	assert.EqualValues(t, "this line has an error", err.Error())
}
func TestImportNotEnoughElements(t *testing.T) {
	// initialize
	mock = &MockProblemsReaderStruct{lines: 1}
	mock.Call = func() (record []string, err error) {
		return []string{"question?"}, nil
	}

	// execute
	importer := NewProblemsStruct(mock)
	records, err := importer.Import()

	// assert
	assert.NotNil(t, err)
	assert.EqualValues(t, "A line doesn't contain enough elements", err.Error())
	assert.Nil(t, records)
}
func TestImportNoAnswer(t *testing.T) {
	// initialize
	mock = &MockProblemsReaderStruct{lines: 1}
	mock.Call = func() (record []string, err error) {
		return []string{"question?", ""}, nil
	}

	// execute
	importer := NewProblemsStruct(mock)
	records, err := importer.Import()

	// assert
	assert.NotNil(t, err)
	assert.EqualValues(t, "A question or an answer is empty", err.Error())
	assert.Nil(t, records)
}
func TestImportValid(t *testing.T) {
	// initialize
	mock = &MockProblemsReaderStruct{lines: 1}
	mock.Call = func() (record []string, err error) {
		return []string{"question?", "answer"}, nil
	}

	// execute
	importer := NewProblemsStruct(mock)
	records, err := importer.Import()

	// assert
	assert.Nil(t, err)
	assert.NotNil(t, records)
	assert.EqualValues(t, "question?", records[0][0])
	assert.EqualValues(t, "answer", records[0][1])
}
